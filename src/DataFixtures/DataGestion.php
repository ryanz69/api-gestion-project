<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;
use App\Repository\UserRepository;

class DataGestion extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create("fr_FR");

        for($i = 1; $i <= 20; $i++) {
            $operation = new Operation();
            $operation->setProfit($faker->randomFloat($nbMaxDecimals = 2, $min = -1000000, $max = 1000000));
            $operation->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
            $operation->setDate($faker->dateTimeThisYear($max = 'now', $timezone = 'Europe/Paris'));

            $manager->persist($operation);
        }

        $manager->flush();
    }
}
