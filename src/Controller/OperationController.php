<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OperationRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Operation;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormErrorIterator;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
* @Route("/api/operation", name="api_operation")
*/
class OperationController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/user", methods="GET")
     */
    public function index(OperationRepository $repo)
    {
        $user = $this->getUser();
        $tab = $repo->findInOrder($user);
        $jsonContent = $this->serializer->normalize($tab, null, ['attributes' =>['id', 'description', 'profit', 'date', 'tag']]);
        return JsonResponse::fromJsonString($this->serializer->serialize($jsonContent, "json"));
    }

    /**
     * @Route("/user/search", methods="POST")
     */
    public function search(Request $request, OperationRepository $repo)
    {
        $content = json_decode($request->getContent(), true);

        $user = $this->getUser();
        $tab = $repo->findBetweenDate($user, new \DateTime($content["dateStart"]), new \DateTime($content["dateEnd"]));

        $jsonContent = $this->serializer->normalize($tab, null, ['attributes' =>['id', 'description', 'profit', 'date', 'tag']]);
        return JsonResponse::fromJsonString($this->serializer->serialize($jsonContent, "json"));
    }

    /**
     * @Route("/user/add", methods="POST")
     */
    public function add(Request $request, ObjectManager $manager)
    {
        $content = json_decode($request->getContent(), true);

        $operation = new Operation();

        $operation->setDescription($content["description"]);
        $operation->setDate(new \DateTime());
        $operation->setProfit($content["profit"]);
        $operation->setTag($content["tag"]);
        $operation->setUser($this->getUser());


        $manager->persist($operation);
        $manager->flush();

        //$jsonContent = $this->serializer->serialize($operation->getId(), 'json');

        $jsonContent = $this->serializer->normalize($operation, null, ['attributes' =>['id', 'description', 'profit', 'date', 'tag']]);

        return JsonResponse::fromJsonString($this->serializer->serialize($jsonContent, "json"));
    }

    /**
     * @Route("/user/remove/{id}", methods="DELETE")
     */
    public function remove(Operation $operation, ObjectManager $manager) {
        $jsonContent = $operation->getId();
        $user = $this->getUser();
        $verifUser = $operation->getUser();
        if($user->getId() == $verifUser->getId()) {
            $manager->remove($operation);
            $manager->flush();
        }
        return JsonResponse::fromJsonString($this->serializer->serialize($jsonContent, "json"));
    }

    private function listFormErrors(FormErrorIterator $errors) {
        $list = [];
        forEach($errors as $error) {
            $list[] = $error->getMessage();
        }
        return $list;
    }
}
