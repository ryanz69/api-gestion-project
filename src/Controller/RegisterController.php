<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends Controller
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $content = json_decode($request->getContent(), true);

        $user = new User();

        $user->setUsername($content["username"]);
        $encoded = $encoder->encodePassword($user, $content["password"]);
        $user->setPassword($encoded);

        $manager->persist($user);
        $manager->flush();
        
        $jsonContent = $this->serializer->serialize($user->getUsername(), 'json');

        return JsonResponse::fromJsonString($jsonContent);
    }
}
